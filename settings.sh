APP=mailpile

### Docker settings.
IMAGE=mailpile
CONTAINER=mailpile

DOMAIN="webmail.example.org"

### Email needed for getting a letsencrypt ssl cert.
SSL_CERT_EMAIL=admin@example.org
