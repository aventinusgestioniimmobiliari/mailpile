# Running Mailpile From a Container

https://www.mailpile.is/

## Installation

  - First install `ds` and `wsproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/wsproxy#installation

  - Then get the scripts: `ds pull mailpile`

  - Create a directory for the container: `ds init mailpile @webmail.example.org`

  - Fix the settings: `cd /var/ds/webmail.example.org/ ; vim settings.sh`

  - Make the container: `ds make`
    
  - If the domain is not a real one (for example if installed on a local machine),
    add to `/etc/hosts` the line `127.0.0.1 webmail.example.org`.
    Then open in browser: https://webmail.example.org

  - Create some users: `ds user start user1 user2 user3`. Each user
    should go to https://webmail.example.org and set a password,
    customize security settings, add an email account (for example
    gmail), etc.

## Manage users

The users can be started (created) or deleted with `ds user`. The user
data are on the directory **accounts/**, which is shared between the
host and the container. In case the container is deleted or rebuilt,
the user accounts remain intact.

## Rebuild the container

In case a new version of the Mailpile package is released, you can
update by rebuilding the container. This can be done by the command:
`ds remake`

## Other commands

```
ds stop
ds start
ds shell
ds help
ds make
```

## Some other docs

- https://www.digitalocean.com/community/tutorials/how-to-install-mailpile-on-ubuntu-14-04
- https://xo.tc/setting-up-mailpile-on-debian-8-jessie-for-remote-access.html
- https://github.com/mailpile/Mailpile/tree/master/shared-data/multipile
