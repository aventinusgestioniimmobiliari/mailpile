cmd_gpg_help() {
    cat <<_EOF
    gpg <user> [<gpg-commands-and-options>...]
        Run a gpg command for the given user.

_EOF
}

cmd_gpg() {
    local user=$1; shift
    [[ -n $user ]] || fail "Error: Missing user.\nUsage:\n$(cmd_gpg_help)"
    [[ -d accounts/$user ]] || fail "Error: User '$user' does not exist."
    local gnupghome="/home/$user/.local/share/Mailpile/default"
    ds exec su - $user -c "gpg --homedir=$gnupghome $@"
}
