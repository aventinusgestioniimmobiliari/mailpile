cmd_user_help() {
    cat <<_EOF
    user [list | start | stop | del ] [<user>...]
        Manage user accounts.

_EOF
}

cmd_user() {
    [[ -z $1 ]] && echo -e "Usage:\n$(cmd_user_help)" && return
    ds inject user.sh "$@"
}
