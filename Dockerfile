include(buster)

RUN apt update &&\
    apt upgrade --yes

### install mailpile and mailpile-apache2
RUN apt install --yes curl apt-transport-https gnupg
RUN curl -s https://packages.mailpile.is/deb/key.asc | apt-key add -
RUN echo "deb https://packages.mailpile.is/deb release main" > /etc/apt/sources.list.d/000-mailpile.list
RUN apt update &&\
    apt install --yes mailpile mailpile-apache2

### install other tools
#RUN apt install --yes vim git wget unzip
