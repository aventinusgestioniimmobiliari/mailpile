#!/bin/bash

#source /host/settings.sh

rm /etc/skel/.[^.]*

ls accounts/ | \
    while read user; do
        adduser $user --no-create-home --disabled-password --gecos ''
        /usr/share/mailpile/multipile/mailpile-admin.py --user $user --start
    done
