#!/bin/bash -x

### get the DOMAIN
source /host/settings.sh

### create the apache2 config file
cat <<EOF > /etc/apache2/sites-available/mailpile.conf
<VirtualHost *:80>
        ServerName $DOMAIN
        RedirectPermanent / https://$DOMAIN/
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $DOMAIN

        RewriteEngine On
        RewriteMap mailpile_u2hp "txt:/var/lib/mailpile/apache/usermap.txt"

        Alias "/default-theme" "/usr/share/mailpile/default-theme"
        <Directory "/usr/share/mailpile/default-theme">
            Require all granted
        </Directory>
EOF
cat <<'EOF' >> /etc/apache2/sites-available/mailpile.conf
        DocumentRoot /usr/share/mailpile/multipile/www
        <Directory /usr/share/mailpile/multipile/www>
            AllowOverride All
            Options FollowSymLinks ExecCGI
            AddHandler cgi-script .cgi
            LogLevel alert rewrite:trace8
            Require all granted

            # Show a helpful error if we're incorrectly configured
            RewriteCond ${mailpile_u2hp:apache_map_test} !=ok
            RewriteCond %{REQUEST_URI} !.*/apache-broken.html$
            RewriteRule .* /apache-broken.html [L,R=302,E=nolcache:1]

            # Redirect users
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^([^/]+)(/.*) http://${mailpile_u2hp:$1}/$1$2 [L,P,QSA]

            # Redirect any proxy errors or 404 errors to our login page
            ErrorDocument 503 /not-running.html
            ErrorDocument 502 /not-running.html
            ErrorDocument 404 /not-running.html
            RewriteRule ^not-running.html / [L,R=302,E=nolcache:1]

            # Avoid caching our error pages
            Header always set Cache-Control "no-store, no-cache, must-revalidate" env=nocache
            Header always set Expires "Thu, 01 Jan 1970 00:00:00 GMT" env=nocache
        </Directory>

        SSLEngine on
        SSLCertificateFile  /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
</VirtualHost>
EOF

### enable ssl, disable mailpile config, enable mailpile site
a2enmod ssl
a2dissite 000-default
a2disconf mailpile
a2ensite mailpile

### reload apache2 configuration
systemctl reload apache2

### change the default webroot from '/mailpile' to ''
sed -i /usr/share/mailpile/multipile/mailpile-admin.py \
    -e '/^APACHE_DEFAULT_WEBROOT/ c APACHE_DEFAULT_WEBROOT = ""    # it was "/mailpile"'
